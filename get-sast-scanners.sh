#!/bin/bash -eu

# get-sast-scanners.sh
#
# Alternative downloader script for the GitLab SAST scanner images,
# compare <https://docs.gitlab.com/ee/user/application_security/sast/index.html#running-sast-in-an-offline-environment>

source $(dirname $0)/downloader_commons.sh

GLOBAL_IMAGE_TAG="4"

SAST_IMAGES="brakeman flawfinder kubesec nodejs-scan phpcs-security-audit pmd-apex semgrep sobelow spotbugs"

for container_image in ${SAST_IMAGES} ; do
  download_image ${container_image}
done

