# downloader-commons.sh
# common defaults and functions for the 

GITLAB_REGISTRY_HOST="registry.gitlab.com"
SECURITY_PRODUCTS_PATH="security-products"
SECURE_ANALYZERS_PREFIX="${GITLAB_REGISTRY_HOST}/${SECURITY_PRODUCTS_PATH}"
PRIVATE_REGISTRY_PREFIX="${PRIVATE_REGISTRY:-your.private.registry}/${SECURITY_PRODUCTS_PATH}"
GLOBAL_IMAGE_TAG="2"

download_image() {
  # Function for downloading an image using skopeo
  # :param $1: the image name, optionally suffixed by the tag.
  #            If no tag is specified here, the $GLOBAL_IMAGE_TAG
  #            will be used.
  image_name=$(awk -F : '{ print $1 }'<<<"$1")
  image_tag=$(awk -F : '{ print $2 }'<<<"$1")
  if [ -z "${image_tag}" ] ; then
    image_tag="${GLOBAL_IMAGE_TAG}"
  fi
  image_file_name="$(tr / _ <<<"${image_name}")_${image_tag}.tar"
  public_registry_image=${SECURE_ANALYZERS_PREFIX}/${image_name}:${image_tag}
  private_registry_image=${PRIVATE_REGISTRY_PREFIX}/${image_name}:${image_tag}
  if [ "x${SHOW_IMAGES:-false}" == "xtrue" ] ; then
    echo "${public_registry_image}"
  elif [ "x${SKIP_DOWNLOAD:-false}" != "xtrue" ] ; then
    echo "Downloading ${public_registry_image} and saving it as ${image_file_name} ..."
    skopeo copy docker://${public_registry_image} docker-archive:${image_file_name}
    if [ $? -gt 0 ] ; then
      echo "Error downloading ${public_registry_image}"
      exit 1
    fi
    echo "Successfully downloaded ${public_registry_image}"
    echo "Upload it to the private registry using"
    echo "  skopeo copy docker-archive:${image_file_name} docker://${private_registry_image}"
    echo "---------------------------------------------------------------"
  elif [ -f ${image_file_name} ] ; then
    echo "  skopeo copy docker-archive:${image_file_name} docker://${private_registry_image}"
  fi
}

