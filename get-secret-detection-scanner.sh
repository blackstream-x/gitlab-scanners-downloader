#!/bin/bash -eu

# get-secret-detection-scanner.sh
#
# Alternative downloader script for the GitLab secret detection scanner images,
# compare <https://docs.gitlab.com/ee/user/application_security/secret_detection/index.html#running-secret-detection-in-an-offline-environment>

source $(dirname $0)/downloader_commons.sh

download_image secrets:5

