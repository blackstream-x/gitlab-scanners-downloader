# gitlab-scanners-downloader

Alternative Bash scripts for downloading GitLab security scanner images,
compare
<https://docs.gitlab.com/ee/user/application_security/offline_deployments/#alternate-way-without-the-official-template>

Requires [skopeo](https://github.com/containers/skopeo).

## Usage

Call the appropriate `get-<scanner>.sh` scripts.

After each successful download, the script suggests an upload command using `skopeo`.

If you would like to have `your.private.registry` changed to the actual name
of your internal registry (and possibly an extra internal path),
just set the environment variable `PRIVATE_REGISTRY` accordingly.

In case you already downloaded the images, you can set the environment variable
`SKIP_DOWNLOAD` to `true`,
which will cause the scripts to just display the upload commands
for all downloaded files.

As an aternative, you can also set the environment variable `SHOW_IMAGES` to `true`.
In that case, sthe scripts will only show the image names you need to download.

