#!/bin/bash -eu

# get-container-scanners.sh
#
# Alternative downloader script for GitLab container scanner images,
# compare <https://docs.gitlab.com/ee/user/application_security/container_scanning/index.html#running-container-scanning-in-an-offline-environment>

source $(dirname $0)/downloader_commons.sh

GLOBAL_IMAGE_TAG="6"

download_image container-scanning
download_image container-scanning/grype
download_image container-scanning/trivy

