#!/bin/bash -eu

# get-iac-scanner.sh
#
# Alternative downloader script for the GitLab IaC security scanner images,
# compare <https://docs.gitlab.com/ee/user/application_security/iac_scanning/>

source $(dirname $0)/downloader_commons.sh

download_image kics:4

